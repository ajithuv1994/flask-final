FROM python:3
ENV src /app
RUN mkdir $src
WORKDIR $src
EXPOSE 5000
COPY requirements.txt .
RUN pip install -r  requirements.txt
COPY . .
CMD ["sh" , "run_app_dev.sh"]

